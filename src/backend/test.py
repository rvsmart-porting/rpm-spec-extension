import os
import time

from tqdm import tqdm
from antlr4 import FileStream

from pcc.parse import parse

folder = os.path.join(os.path.dirname(__file__), 'rpm-tests')
filenames = os.listdir(folder)
failed_count = 0
for filename in tqdm(filenames):
    path = os.path.join(folder, filename)
    a = parse(FileStream(path))
    if a.failed:
        print('='*40+f'\nParse {filename}\n'+'='*40)
        print('Text:')
        with open(path) as f:
            print(''.join(f'{line_no+1: 3d} | {txt}' for line_no, txt in enumerate(f.readlines())))
        print('-'*40)
        print('\n'.join(a.result))
        failed_count += 1
        input('...')
print(f'Parse success: {len(filenames) - failed_count} / {len(filenames)}')
