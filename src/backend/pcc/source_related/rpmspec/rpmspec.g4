grammar rpmspec;

specFile
    : startSection? section* EOF
    ;

section
    : packageSection
    | otherSection
    | changelogSection
    ;


startSection
    : (statementLine | emptyLine)+
    ;

otherSection
    : '%' SHELL_SECTIONS EOL? (statementLine | emptyLine)+
    ;

packageSection
    : (packageInfoLine | emptyLine)+
    ;

changelogSection
    : '%' 'changelog' EOL (statementLine | emptyLine)*
    ;

statementLine
    : SPACE? statement SPACE? EOL
    ;

packageInfoLine
    : anyIdentifier+ ':' SPACE? statement EOL
    ;

emptyLine
    : SPACE? EOL
    ;

multipleLineStatements
    : (SPACE? statement? SPACE? EOL)* SPACE? statement
    ;

statement
    : defineMacro
    | undefineMacro
    | anything
    ;

defineMacro
	: '%' 'define' SPACE anyIdentifier ('(' anyWord* ')')? SPACE anything  # MacroDefine
	;

undefineMacro
    : '%undefined' anyIdentifier  # MacroUndefine
    ;

builtinMacro
    : '%' (DIRECT_OR_BRACKET_BUILTIN_MACRO | DIRECT_BUILTIN_MACRO) SPACE statement
    | '%{' (DIRECT_OR_BRACKET_BUILTIN_MACRO | BRACKET_BUILTIN_MACRO) ':' multipleLineStatements RB
    ;

condition
    : '%{' MACRO_CONDITION SPACE anyIdentifier RB
    ;

shellLikeMacro
    : '%' INTEGER_LITERAL
    | '%*'
    | '%**'
    | '%#'
    | '%{-f}'
    | '%{-f*}'
    ;

referenceMacro
    : '%' anyIdentifier  # DirectMacroRef
    | '%{' anyIdentifier RB  # BracketMacroRef
    | ('%{?' | '%{!?' | '%{?!') anyIdentifier (':' statement)? RB  # ConditionallyMacroRef
    ;

wordOrMacro
    : condition
    | referenceMacro
    | builtinMacro
    | shellLikeMacro
    | anyWord
    ;

anyWord
	: anyIdentifier
	| SHELL_SECTIONS
	| BOOL_LITERAL
	| FLOAT_LITERAL
	| INTEGER_LITERAL
	| CODE
	| ':'
	| '%%'
	| '-'
	;

anyIdentifier
    : IDENTIFIER
    | DIRECT_OR_BRACKET_BUILTIN_MACRO
    | DIRECT_BUILTIN_MACRO
    | BRACKET_BUILTIN_MACRO
    | SHELL_SECTIONS
    | MACRO_CONDITION
    ;

anything
	: (wordOrMacro | SPACE | '?' | '!' | '(' | ')' | '<' | '>' | LB | RB | '\\' | '$')+
	;

INTEGER_LITERAL: NUMERIC+;
FLOAT_LITERAL: INTEGER_LITERAL '.' INTEGER_LITERAL;
DIRECT_OR_BRACKET_BUILTIN_MACRO: 'getncpus' | 'verbose';
DIRECT_BUILTIN_MACRO: 'if' | 'elif' | 'endif' | 'global'
    | 'getconfdir' | 'rpmversion' | 'trace' | 'dump';
BRACKET_BUILTIN_MACRO: 'load' | 'expand' | 'expr' | 'lua' | 'macrobody' | 'quote'
    | 'gsub' | 'len' | 'lower' | 'rep' | 'reverse' | 'sub' | 'upper' | 'shescape' | 'shrink'
    | 'basename' | 'dirname' | 'exists' | 'suffix' | 'url2path' | 'uncompress'
    | 'getenv'
    | 'echo' | 'warn'
    | 'S' | 'P'
    ;
SHELL_SECTIONS: 'generate_buildrequires'|'prep'|'build'|'install'|'check'|'post'|'postun'
    | 'posttrans'|'pre'|'preun'|'pretrans'|'clean'|'verifyscript'|'triggerpostun'|'triggerprein'
    | 'triggerun'|'triggerin'|'trigger'|'filetriggerin'|'filetrigger'|'filetriggerun'
    | 'filetriggerpostun'|'transfiletriggerin'|'transfiletrigger'|'transfiletriggerun'
    | 'transfiletriggerpostun' | 'description'
    ;
MACRO_CONDITION: 'defined' | 'undefined' | 'with';
IDENTIFIER: (ALPHA|NUMERIC|'_')+;

CODE: (~[\r\n \t\f{}()%:?!<>0-9a-zA-Z])+;
ALPHA: [a-zA-Z];
NUMERIC: [0-9];
LB: '{';
RB: '}';
SPACE: [ \t]+;
EOL: '\r'? '\n' | '\r';
BOOL_LITERAL: 'true' | 'false';

LINE_COMMENT: '#' ~[\r\n]* EOL -> skip;
BSNL : '\\' [ \t]* EOL -> skip;
