from antlr4 import ParseTreeWalker, CommonTokenStream, TerminalNode, ErrorNode, ParserRuleContext, ParseTreeListener, \
    InputStream

from pcc.antlr.build import update
from pcc.source_related.rpmspec.rpmspecLexer import rpmspecLexer
from pcc.source_related.rpmspec.rpmspecParser import rpmspecParser


class A(ParseTreeListener):

    def __init__(self):
        self.failed = False
        self.result = []

    def visitTerminal(self, node: TerminalNode):
        # print(f'terminal [{node}]')
        pass

    def visitErrorNode(self, node: ErrorNode):
        self.result.append(f'error {node}')
        self.failed = True

    def enterEveryRule(self, ctx: ParserRuleContext):
        # print(f'enter', ctx)
        pass

    def exitEveryRule(self, ctx: ParserRuleContext):
        # print(f'exit', ctx)
        pass

    def enterMacroDefine(self, ctx: ParserRuleContext):
        self.result.append(f'Define {list(map(lambda x: x.getText(), ctx.children))}')

    def enterMacroUndefine(self, ctx: ParserRuleContext):
        self.result.append(f'Undefine {list(map(lambda x: x.getText(), ctx.children))}')

    def enterBracketMacroRef(self, ctx: ParserRuleContext):
        self.result.append(f'Bracket Ref {list(map(lambda x: x.getText(), ctx.children))}')

    def enterDirectMacroRef(self, ctx: ParserRuleContext):
        self.result.append(f'Direct Ref {list(map(lambda x: x.getText(), ctx.children))}')


updated = False


def parse(input_stream: InputStream):
    global updated
    if not updated:
        update('rpmspec', verbose=2, force_update=False)
        updated = True
    ast = rpmspecParser(CommonTokenStream(rpmspecLexer(input_stream))).specFile()
    # print('AST:')
    # print(ast.toStringTree().replace('\\n', '\n'))
    walker = ParseTreeWalker()
    a = A()
    walker.walk(a, ast)
    return a
