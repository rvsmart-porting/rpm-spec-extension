import os
import time

from loguru import logger

from pcc.base import *

antlr_alias = f'java -Xmx500M -cp "{os.path.join(lib_dir, "antlr4-4.13.1-complete.jar")}" org.antlr.v4.Tool'


def language_folder(language: str):
    return os.path.join(lib_dir, 'source_related', language)


def grammar_file_path(language: str):
    return os.path.join(language_folder(language), f'{language}.g4')


def generated_files_path(language: str):
    return [os.path.join(language_folder(language), filename) for filename in [
        f'{language}Lexer.py', f'{language}Parser.py', f'{language}Listener.py'
    ]]


def build(language: str, verbose: int = 0):
    if verbose > 1:
        logger.debug('Run ANTLR:')
        logger.debug(f'    ANTLR alias is: {antlr_alias}')
    cmd = f'{antlr_alias} -Dlanguage=Python3 {grammar_file_path(language)}'
    if verbose > 1:
        logger.debug(f'    Run build command: {cmd}')
    os.system(cmd)
    if verbose > 1:
        logger.debug('Build finished.')


def clean(language: str, verbose: int = 0):
    if verbose > 1:
        logger.debug('Clean existing files:')
    to_remove = list(map(lambda x: os.path.join(language_folder(language), x),
                         filter(lambda x: x.split('.')[-1] not in ['g4', 'py'], os.listdir(language_folder(language)))))
    to_remove += generated_files_path(language)
    for path in to_remove:
        if verbose > 1:
            logger.debug(f'    Remove file {path}')
        if os.path.isfile(path):
            os.remove(path)
    if verbose > 1:
        logger.debug('Clean finished.')


def up_to_date(language, verbose=0):
    if not all(map(lambda x: os.path.exists(x), generated_files_path(language))):
        if verbose > 1:
            logger.debug("Some generated file doesn't exist, build isn't up to date.")
        return False
    gen_time = min(map(lambda x: os.stat(x).st_mtime, generated_files_path(language)))
    grammar_time = os.stat(grammar_file_path(language)).st_mtime
    if verbose > 1:
        logger.debug(
            f'Generated files earliest updated time: {time.strftime("%Y%m%d-%H:%M:%S", time.localtime(gen_time))}')
        logger.debug(f'Grammar file updated time: {time.strftime("%Y%m%d-%H:%M:%S", time.localtime(grammar_time))}')
    if gen_time < grammar_time:
        if verbose > 1:
            logger.debug("Grammar file updated later, need to update.")
        return False
    if verbose > 1:
        logger.debug("Grammar file updated earlier, build is up to date.")
    return True


def update(language, verbose=0, force_update=False):
    if not force_update and up_to_date(language, verbose=verbose):
        if verbose > 0:
            logger.debug('Build already up to date.')
        return
    if verbose > 0:
        logger.debug('Rebuilding...')
    clean(language, verbose=verbose)
    build(language, verbose=verbose)
    if verbose > 0:
        logger.debug('Rebuild finished.')
    logger.info('Rebuilt code, Please run the program again to compile.')
    exit(1)
