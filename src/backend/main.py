import argparse
import os

from antlr4 import FileStream, CommonTokenStream, ParseTreeWalker, ParseTreeListener, TerminalNode, ErrorNode, \
    ParserRuleContext
from loguru import logger

from pcc.parse import parse
from pcc.source_related.JSON.JSONLexer import JSONLexer
from pcc.source_related.JSON.JSONParser import JSONParser
from pcc.source_related.rpmspec.rpmspecLexer import rpmspecLexer
from pcc.source_related.rpmspec.rpmspecParser import rpmspecParser

parser = argparse.ArgumentParser()
parser.add_argument('input', help='Path to the package config file')
parser.add_argument('-s', '--source', choices=['JSON', 'rpmspec'], required=False, help='Source language')
parser.add_argument('-o', '--output', required=False, help='Path to the output file')

parser.add_argument('--tokens', action='store_true', help='Only lex and print tokens instead of compile')
parser.add_argument('-r', '--run', action='store_true', help='Run after compile')
parser.add_argument('-v', '--verbose', action='count', default=0, help='Verbosity')

args = parser.parse_args()

if not os.path.isfile(args.input):
    logger.critical(f'No such file: {args.input}')
    exit(1)
name, sep, extension = args.input.rpartition('.')
if args.source is None and sep == '.':
    source = extension
input_stream = FileStream(args.input)

parse(input_stream)
